const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const User = require('../models/userModel');

class AuthController {
  async register(req, res) {
    try {
      const {username, password} = req.body;
      const newUser = await User.findOne({username});
      if (newUser) {
        return res.status(400)
            .send({message: 'User already exists! Please login!'});
      }

      const user = new User({
        username: username,
        password: await bcrypt.hash(password, 10),
      });

      await user.save();

      res.status(200).send({message: 'Success'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }

  async login(req, res) {
    try {
      const {username, password} = req.body;
      const user = await User.findOne({username});

      if (!user) {
        return res.status(400)
            .send({message: 'Invalid username'});
      }

      const checkPassword = await bcrypt.compare(password, user.password);

      if (!checkPassword) {
        return res.status(400)
            .send({message: 'Invalid password'});
      }

      const token = jwt.sign({
        _id: user._id,
        username: user.username,
      }, process.env.SECRET_KEY);

      res.status(200).send({message: 'Success', jwt_token: token});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }
}

module.exports = new AuthController();
