const bcrypt = require('bcrypt');

const User = require('../models/userModel');

class UserController {
  async getUser(req, res) {
    try {
      const userId = req.user.userId;
      const userInfo = await User.findOne({_id: userId});

      const userAbout = {
        _id: userInfo._id,
        username: userInfo.username,
        createdDate: userInfo.createdDate,
      };
      res.status(200).send({user: userAbout});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }

  async deleteUser(req, res) {
    try {
      const userId = req.user.userId;
      await User.findByIdAndRemove(userId);
      res.status(200).send({message: 'Success'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }

  async changeUserPassword(req, res) {
    try {
      const userId = req.user.userId;
      const {oldPassword, newPassword} = req.body;
      const user = await User.findOne({_id: userId});

      const checkOldPassword = await bcrypt.compare(oldPassword, user.password);

      if (!checkOldPassword) {
        return res.status(400)
            .send({message: 'Invalid old password'});
      }

      const newPasswordHash = await bcrypt.hash(newPassword, 10);

      await User.updateOne({_id: userId}, {password: newPasswordHash});

      res.status(200).send({message: 'Success'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }
}

module.exports = new UserController();
