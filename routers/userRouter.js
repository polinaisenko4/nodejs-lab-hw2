const express = require('express');
const userController = require('../controllers/userController');

const router = express.Router();

router.get('/me', userController.getUser);
router.delete('/me', userController.deleteUser);
router.patch('/me', userController.changeUserPassword);

module.exports = router;
