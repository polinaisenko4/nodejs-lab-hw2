const express = require('express');
const router = express.Router();

const notesController = require('../controllers/notesController');

router.get('/:id', notesController.getUserIdNote);
router.put('/:id', notesController.updateUserIdNote);
router.patch('/:id', notesController.checkUserIdNote);
router.delete('/:id', notesController.deleteUserIdNote);
router.post('/', notesController.postUserNote);
router.get('/', notesController.getUserNotes);

module.exports = router;
